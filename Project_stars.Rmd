---
title: "Database on stars"
output: html_notebook
---

# Data visualization

```{r}
install.packages("questionr", dep=TRUE)
```
```{r}
starsDataset <- read.csv("C:/Users/leles/OneDrive/Documents/MLDM/S2/Data Mining/Project/class.csv")
View(starsDataset)
```
# Data Analysis
```{r}
names(starsDataset)
```
```{r}
str(starsDataset)
```
```{r}
summary(starsDataset)
```

# Quantitative values

```{r}
hist(starsDataset$Temperature..K., col = "purple", border = "white",
         main = paste("Repartition of Temperature"),
         xlab = "temperature (K)", ylab = "count", 
         ylim = c(0, 240),
         xlim = c(1939, 40000),
         labels = TRUE)
```
```{r}
hist(starsDataset$Luminosity.L.Lo., col = "purple", border = "white",
         main = paste("Repartition of Luminosity"),
         xlab = "luminosity (L/Lo)", ylab = "count", 
         ylim = c(0, 240),
         xlim = c(0.0, 849420.0),
         labels = TRUE)
```
```{r}
hist(starsDataset$Radius.R.Ro., col = "purple", border = "white",
         main = paste("Repartition of Radius"),
         xlab = "radius (R/Ro)", ylab = "count", 
         ylim = c(0, 240),
         xlim = c(0.0084, 1948.5000),
         labels = TRUE)
```
```{r}
hist(starsDataset$Absolute.magnitude.Mv., col = "purple", border = "white",
         main = paste("Repartition of Absolute Magnitude"),
         xlab = "absolute magnitude (Mv)", ylab = "count", 
         ylim = c(0, 240),
         xlim = c(-11.920, 20.060),
         labels = TRUE)
```
# Categorical values

```{r}
library(ggplot2)
ggplot(starsDataset, aes(x=Star.color)) +
  geom_bar(fill="violet")+
  coord_flip()+
  ggtitle("Repartition of Star's Color") +
  xlab("color") + ylab("count")+
  theme(plot.title = element_text(color="black", size=15, face="bold"))
```

```{r}
library(ggplot2)
ggplot(starsDataset, aes(x=Spectral.Class)) +
  geom_bar(fill="violet")+
  coord_flip()+
  ggtitle("Repartition of Star's Class") +
  xlab("spectral class") + ylab("count")+
  theme(plot.title = element_text(color="black", size=15, face="bold"))
```
```{r}
library(ggplot2)
ggplot(starsDataset, aes(x=Star.type)) +
  geom_bar(fill="violet")+
  coord_flip()+
  ggtitle("Repartition of Star's Type") +
  xlab("type") + ylab("count")+
  theme(plot.title = element_text(color="black", size=15, face="bold"))
```
## Correlation

```{r}
install.packages("corrplot")
install.packages("ggplot2")
```

```{r}
library(corrplot)
M<-cor(starsDataset[, unlist(lapply(starsDataset, is.numeric))]) 
head(round(M,2))
corrplot(M, method="circle")
```
```{r}
ggplot(data = starsDataset) + geom_point(aes(x = Absolute.magnitude.Mv., y = log2(Luminosity.L.Lo.), color = Star.type))+
  ggtitle("Absolute Magnitude according to the Luminosity") +
  xlab(" absolute magnitude (Mv)") + ylab("log2(luminosity (L/Lo))")+
  theme(plot.title = element_text(color="black", size=15, face="bold"))
```

```{r}
ggplot(data = starsDataset) + geom_point(aes(x = Temperature..K., y = log2(Radius.R.Ro.), color = Star.type))+
  ggtitle("Temperature according to the Radius") +
  xlab("temperature (K)") + ylab("log2(radius (R))")+
  theme(plot.title = element_text(color="black", size=15, face="bold"))
```
```{r}
ggplot(data = starsDataset) + geom_point(aes(x = Temperature..K., y = log2(Luminosity.L.Lo.), color = Star.type))+
  ggtitle("Temperature according to the Luminosity") +
  xlab("temperature (K)") + ylab("log2(luminosity (L/Lo))")+
  theme(plot.title = element_text(color="black", size=15, face="bold"))
```
```{r}
ggplot(data = starsDataset) + geom_point(aes(x = Temperature..K., y = Absolute.magnitude.Mv., color = Star.type)) + scale_y_reverse()+
  ggtitle("Temperature according to the Absolute Magnitude") +
  xlab("temperature (K)") + ylab("absolute magnitude (Mv)")+
  theme(plot.title = element_text(color="black", size=15, face="bold"))
```

```{r}
hr <- ggplot(data = starsDataset) + geom_point(aes(x = Temperature..K., y = Absolute.magnitude.Mv., color = Star.type, shape = Spectral.Class)) +
  scale_shape_manual(values =  c(4,15,17,18,21,22,25)) + scale_y_reverse() + scale_x_reverse() +
  labs(title = "H-R Diagram", x = "temperature (K)", y ="absolute magnitude (Mv)") +
  theme(plot.title = element_text(color = "blue", face = "bold", hjust = 0.5))
hr
```
# K-NN

## Normalization
```{r}
set.seed(123)
ran <- sample(1:nrow(starsDataset), 0.8 * nrow(starsDataset))
nor <-function(x) { (x -min(x))/(max(x)-min(x))   }
stars_norm <- as.data.frame(lapply(starsDataset[,c(1,2,3,4)], nor))

summary(stars_norm)
```
```{r}
stars_train <- stars_norm[ran,]

stars_test <- stars_norm[-ran,]

stars_target_category <- starsDataset[ran,5]

stars_test_category <- starsDataset[-ran,5]

library(class)

pr <- knn(stars_train,stars_test,cl=stars_target_category,k=70)

tab <- table(pr,stars_test_category)

accuracy <- function(x){sum(diag(x)/(sum(rowSums(x)))) * 100}
 accuracy(tab)
```
```{r}
i=1                          
k.optm=1                     
for (i in 1:100){ 
    knn.mod <-  knn(stars_train,stars_test,cl=stars_target_category,k=i)
    k.optm[i] <- 100 * sum(stars_test_category == knn.mod)/NROW(stars_test_category)
    k=i
    cat(k,'=',k.optm[i],'\n')        
}
```
```{r}
plot(k.optm, type="b", xlab="K- Value",ylab="Accuracy level")
```
# Evaluation

```{r}
install.packages('caret')
install.packages('ISLR')
install.packages('reshape2')
install.packages('plyr')
install.packages('dplyr')
install.packages('class')
install.packages("prediction")
```

```{r}
library(caret)
library(ISLR) 
library(ggplot2) 
library(reshape2) 
library(plyr) 
library(dplyr) 
library(class)
library(prediction)
```
#AUC ROC
```{r}
knn_model = knn(stars_train,stars_test,cl=stars_target_category,k=70,prob=TRUE)
prob <- attr(knn_model, "prob")
prob <- 2*ifelse(knn_model == "-1", prob, 1-prob) - 1
pred_knn <- prediction(prob, stars_target_category)
performance_knn <- performance(pred_knn, "tpr", "fpr")
auc_knn <- performance(pred_knn,"auc")@y.values
auc_knn
```

```{r}
plot(performance_knn,col=2,lwd=2,main="ROC Curves for KNN")
```